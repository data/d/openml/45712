# OpenML dataset: doa_bwin_balanced

https://www.openml.org/d/45712

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The balanced dataset of the doa_bwin dataset (ID:45711) using the SMOTE algorithm in the R-Package themis. This dataset represents a 50:50 split between the classes of the DV variable (354/354).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45712) of an [OpenML dataset](https://www.openml.org/d/45712). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45712/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45712/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45712/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

